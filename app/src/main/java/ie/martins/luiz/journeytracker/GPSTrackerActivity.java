package ie.martins.luiz.journeytracker;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

// ---- That is the class definition ----
public class GPSTrackerActivity extends LinearLayout {

    // ---- Required constructor that takes in a context ----
    public GPSTrackerActivity(Context c) {
        super(c);
        init();
    }

    // ---- Required constructor that takes in an attribute set ----
    public GPSTrackerActivity(Context c, AttributeSet as) {
        super(c, as);
        init();
    }
    // ---- Required constructor that takes in a style ----
    public GPSTrackerActivity(Context c, AttributeSet as, int default_style) {
        super(c, as, default_style);
        init();
    }
    // ---- Private method that will share initialisation between all three constructors ----
    private void init() {

        // ---- Get access to the layout inflator service and inflate the XML file ----
        LayoutInflater li = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.custom_view_gps_tracker, this, true);


        // ---- Getting access to the TextViews ----
        tv_gps_status = (TextView) findViewById(R.id.tv_gps_status);
        tv_current_speed = (TextView) findViewById(R.id.tv_current_speed);
        tv_average_speed = (TextView) findViewById(R.id.tv_average_speed);
        tv_overall_time = (TextView) findViewById(R.id.tv_overall_time);

        // ---- It will get access to the controls and add a onClickListener ----
        btn_tracking = (Button) findViewById(R.id.btn_tracker);
        btn_tracking.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                // ---- That's the initial state of the button when it's not pressed----
                if ( btn_tracking.getText().toString().equals("START TRACKING")) {
                    // ---- It will erase all arrays, they are limited at 100 indexes ----
                    ar_current_speed.removeAll(ar_current_speed);
                    ar_current_speed_limited.removeAll(ar_current_speed_limited);
                    ar_longitude.removeAll(ar_longitude);
                    ar_latitude.removeAll(ar_latitude);
                    // ---- The text of the button will change when clicked ----
                    btn_tracking.setText("STOP TRACKING");
                    // ---- It will add the location listener ----
                    MainActivity.addLocationListener();
                    // ---- Defining the initial value of the variables as 0 ----
                    // ---- This variable will be the overall time ----
                    int_overall_time=0;
                    // ---- This variable will be used to track the current index on the array, which will be similar to the the overall time until it reach 100
                    int_current_index=0;
                    // ---- Last sum, it will be used for the BigO average ----
                    last_sum=0f;
                    // ---- Incremental sum, it will be used for the BigO average ----
                    inc_sum=0f;
                    // ---- Call the handler which will execute a set of operations every second ----
                    handler.post(timedTask);
                }
                // ----- That is the state of the button when is was pressed for the first time ----
                else if ( btn_tracking.getText().toString().equals("STOP TRACKING")) {
                    // ---- It will change the text of the button ----
                    btn_tracking.setText("START TRACKING");
                    // ---- It will disable the location listener ----
                    MainActivity.locmanager.removeUpdates(MainActivity.ll);
                    // ---- It will stop the handler ----
                    handler.removeCallbacks(timedTask);
                    // ---- It will change the text of the gps status Text View ----
                    tv_gps_status.setText(R.string.tv_gps_status_inactive);
                }
            }
        });
    }

    static TextView tv_gps_status, tv_current_speed, tv_average_speed, tv_overall_time;
    static Button btn_tracking;
    // ---- It will be the overall time integer ----
    static int int_overall_time;
    // ---- It will be the current index similar to the overall time until it reach 100 ----
    static int int_current_index;
    static Handler handler = new Handler();
    static final List<Float> ar_current_speed = new ArrayList<>();
    // ---- The _limited will be a copy of the variable with the same name but the values will be replace by 60(km/h) if more than that ----
    static final List<Float> ar_current_speed_limited = new ArrayList<>();
    static List<Double> ar_latitude = new ArrayList<>();
    static List<Double> ar_longitude = new ArrayList<>();
    static Float last_sum = 0f;
    static Float inc_sum;
    static Float average;
    // ---- The _limited will be a copy of the variable with the same name but the values will be replace by 60(km/h) if more than that ----
    static Float average_limited;

    // ---- The following operations will occur every second ----
    protected Runnable timedTask = new Runnable(){
        @Override
        public void run() {
            handler.postDelayed(timedTask, 1000);
            // ---- It will make the overall time TextView prettier, instead of xxxxx second it will display xhxmxs
            if (int_overall_time < 60) {
                tv_overall_time.setText("Overall Time: " + String.valueOf(int_overall_time) + "s");
            }
            else if (int_overall_time >= 60 && int_overall_time < 3600) {
                tv_overall_time.setText("Overall Time: " + String.valueOf((int_overall_time % 3600) / 60) + "m" + String.valueOf(int_overall_time % 60) + "s");
            }
            else if (int_overall_time >= 3600 && int_overall_time < 86400) {
                tv_overall_time.setText("Overall Time: " + String.valueOf((int_overall_time / 3600)) + "h" + String.valueOf((int_overall_time % 3600) / 60) + "m" + String.valueOf(int_overall_time % 60) + "s");
            }
            // ---- If the variables are null set zero on them ----
            if (MainActivity.float_current_speed == null || MainActivity.double_latitude == null || MainActivity.double_longitude == null )  {
                MainActivity.float_current_speed = 0f;
                MainActivity.double_latitude = 0.0;
                MainActivity.double_longitude = 0.0;
            }
            // ---- Limiting the size of the arrays at 100 index ----
            if ( ar_current_speed.size() < 100) {
                ar_current_speed.add(MainActivity.float_current_speed);
                ar_current_speed_limited.add(MainActivity.float_current_speed);
                // ---- The latitude and longitude is here for future use ----
                ar_latitude.add(MainActivity.double_latitude);
                ar_longitude.add(MainActivity.double_longitude);
                inc_sum = last_sum + ar_current_speed.get(int_current_index);
                average = inc_sum / (int_current_index + 1);
                average_limited = inc_sum / (int_current_index + 1);
                last_sum = inc_sum;
            }
            // ---- If the size is 100 it will grow to 101 and than delete the first position, the current index will be decreased ----
            else {
                ar_current_speed.add(MainActivity.float_current_speed);
                ar_current_speed_limited.add(MainActivity.float_current_speed);

                // ---- That is the average calculation using Big O, keeping track of the last sum to avoid different processing time depending on the size of the array ----
                inc_sum = last_sum + ar_current_speed.get(int_current_index);
                average = inc_sum / (int_current_index + 1);
                average_limited = inc_sum / (int_current_index + 1);
                inc_sum = ((last_sum + ar_current_speed.get(int_current_index)) - ar_current_speed.get(0));
                last_sum = inc_sum;
                average = (inc_sum / (int_current_index));
                average_limited = (inc_sum / (int_current_index));

                ar_current_speed.remove(0);
                ar_current_speed_limited.remove(0);
                // ---- The latitude and longitude is here for future use ----
                ar_latitude.add(MainActivity.double_latitude);
                ar_latitude.remove(0);
                ar_longitude.add(MainActivity.double_longitude);
                ar_longitude.remove(0);
                int_current_index--;
            }
            tv_average_speed.setText("Average Speed: " + String.valueOf(average) + " km/h");
            tv_current_speed.setText("Current Speed: " + String.valueOf(ar_current_speed.get(int_current_index)) + " km/h");
            int_overall_time++;
            int_current_index++;
        }};
}