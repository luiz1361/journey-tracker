package ie.martins.luiz.journeytracker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

// ---- GPS Graph class definition ----
public class GPSGraphActivity extends LinearLayout {

    // ---- It makes the GPSGraphActivity be square in size ----
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size;
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        // ---- The size of the square will be based on the lowest side, width or height ----
        if (width < height) {
            size = width;
        } else  {
            size = height;
        }
        GPSGraphActivity.super.setLayoutParams(new LayoutParams(size, size));
    }

    // ---- Required constructor that takes in a context ----
    public GPSGraphActivity(Context c) {
        super(c);
        init();
    }
    // ---- Required constructor that takes in an attribute set ----
    public GPSGraphActivity(Context c, AttributeSet as) {
        super(c, as);
        init();
    }
    // ---- Required constructor that takes in a style ----
    public GPSGraphActivity(Context c, AttributeSet as, int default_style) {
        super(c, as, default_style);
        init();
    }
    // ---- Private method that will share initialisation between all three constructors ----
    private void init() {

        // ---- Get access to the layout inflator service and inflate the XML file ----
        LayoutInflater li = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.custom_view_graph, this, true);
    }

    // ---- It will be the the horizontal grade lines (grayish) ----
    private final Paint paint_grade_lines = new Paint();
    // ---- It will be the line graph (green) ----
    private final Paint line_graph = new Paint();
    // ---- It will be the average horizontal line (red) ----
    private final Paint average = new Paint();
    @Override
    public void onDraw(Canvas canvas) {

        // ---- Style definitions for the Paint's ----
        paint_grade_lines.setStyle(Paint.Style.STROKE);
        paint_grade_lines.setColor(0x66666666); //(grayish)
        paint_grade_lines.setStrokeWidth(1);

        line_graph.setStyle(Paint.Style.STROKE);
        line_graph.setColor(0xAA00FF00); //(green)
        line_graph.setStrokeWidth(1);

        average.setStyle(Paint.Style.STROKE);
        average.setColor(0xAAFF0000); //(red)
        average.setStrokeWidth(1);

        // ---- It will draw the horizontal lines dividing the height in six equal size parts ----
        for (int p = 0; p < 6; p++ ) {
            canvas.drawLine(0, getHeight() / 6 * p, getWidth(), getHeight() / 6 * p, paint_grade_lines);
        }

        if ( GPSTrackerActivity.int_current_index > 0) {

            // ---- If the current speed is more than 60km/h it will be set to 60km/h(limit of the screen) ----
            // the current speed array itself won't have the values changed.
            if ( GPSTrackerActivity.ar_current_speed_limited.get(GPSTrackerActivity.int_current_index-1).intValue() > 60f) {
                GPSTrackerActivity.ar_current_speed_limited.set(GPSTrackerActivity.int_current_index - 1, 60f);
            }
            // ---- If the average is more than 60km/h it will be set to 60km/h(limit of the screen) ----
            // the average array itself won't have the values changed.
            if (GPSTrackerActivity.average_limited > 60f) {
                GPSTrackerActivity.average_limited = 60f;
            }

            // ---- It will draw the average line based on the "average_limited" variable on the GPSTrackerActivity ----
            canvas.drawLine(0, getHeight() - (getHeight() /60f * GPSTrackerActivity.average_limited), getWidth(), getHeight() - (getHeight() /60f * GPSTrackerActivity.average_limited), average);

            // ---- It will draw the graph line based on the "arr_current_speed_limited" variable on the GPSTrackerActivity ----
            int x=1; float y=0; int j=0;
            for (float  i = 1; i < 100; i++ ){
                if ( GPSTrackerActivity.ar_current_speed_limited.size() > i ) {
                    canvas.drawLine(getWidth() / 99f * y, getHeight() - (getHeight() /60f * (GPSTrackerActivity.ar_current_speed_limited.get(j))), getWidth() / 99f * i, getHeight() - (getHeight() / 60f * (GPSTrackerActivity.ar_current_speed_limited.get(x))), line_graph);
                }
            x++;y++;j++;
            }
        }
        // ---- It will invalidate the onDraw, keeping the Paint's refreshing at a short interval ----
        invalidate();
    }


}